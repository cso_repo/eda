# README #

This repository contains the development R implementation of the Bayesian inference framework used in *Mina et al., Nature Genetics 2020* to study the associations between genomic profiles of cancer samples and their phenotypes.
The methdology is used to study how cancer samples with different genomic profiles (e.g. combinations of mutations or CNAs) respond to functional perturbations (e.g. knock-out of genes) or treatment with compounds.

### What is this repository for? ###

* This repository contains a series of R functions to perform Bayesian hypothesis testing of the association between genomic profiles and changes in cancer cell phenotypes.
* Last Version: 1.0

### How To ###

### Contribution guidelines ###

The methodology is described in [sumbitted manuscript].

### Who do I talk to? ###

* For any question, please contact Marco Mina (marco.mina.85@gmail.com) or Giovanni Ciriello (giovanni.ciriello@unil.ch).
