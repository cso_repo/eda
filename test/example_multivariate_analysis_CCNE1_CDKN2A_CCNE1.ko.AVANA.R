# require(readr)
options(width=310)
require(plyr)
require(dplyr)

# Handy function to plot posteriors
plot_posteriors <- function(B_model) {
	require(BayesFactor)
	require(reshape2)
	require(ggplot2)
	B_model_post = posterior(B_model, iterations=1000)
	B_model_post_alt_class = B_model_post[,grep('alt_class', colnames(B_model_post))]
	B_model_post_alt_class = B_model_post_alt_class[,setdiff(colnames(B_model_post_alt_class), 'g_alt_class')]
	temp = as.data.frame(B_model_post_alt_class)
	# temp$temp=1:nrow(B_model_post_alt_class)
	long = melt(temp)
	long %>% head
	long %>% count(variable)
	g <- ggplot(data = long) + geom_vline(xintercept = 0) + geom_density(aes (x = value, color=variable)) + theme_bw()
	return(g)
}	


# source('R/format_analysis_data.R')
source('../R/multivariate_analysis.R')
source('../R/posthoc_analysis.R')

#######################
# Load data
data = read.table('data/CCNE1_CDKN2A_CCNE1.KO_AVANA_example_v1.txt', sep="\t", stringsAsFactors = FALSE, quote='', header=TRUE)
data %>% head



#######################
# Perform multivariate ANOVA type II and Bayesian analysis using the gene level semantic class as classification feature.

results = basic_symmetric_multivariate_effect_analysis(data, effect_column = 'CCNE1_KO_AVANA_score', class_column = 'alteration_semantic_class', covariates=c('tumor_type'),
	min.samples.per.class=3, do.anova = TRUE, do.bf = TRUE, return.details=TRUE, logging=TRUE)
names(results)

# Let's have a look at the results
results$data %>% head # Data ultimately retained and used in the analysis (head)
results[c('data_stats', 'class_stats')] # Stats on the data
results$log # Log of the filtering procedure (if logging=TRUE)
results[c('stats', 'aov', 'tukey')] # Results of the statistical analysis
B_model = results$bf # The BF model

# Let's generate the plot of the posterior distirbutions
g_class = plot_posteriors(B_model) + ggtitle('Posterior distirbutions of effect size \n Gene level semantic class.') + xlab('Effect size: CCNE1 KO AVANA score')



#######################
# Perform multivariate ANOVA type II and Bayesian analysis using the alteration level semantic class as classification feature (keep CNA and mutation separated).
results = basic_symmetric_multivariate_effect_analysis(data, effect_column = 'CCNE1_KO_AVANA_score', class_column = 'alteration_semantic', covariates=c('tumor_type'),
	min.samples.per.class=3, do.anova = TRUE, do.bf = TRUE, return.details=TRUE, logging=TRUE)
names(results)

# Let's have a look at the results
results$data %>% head # Data ultimately retained and used in the analysis (head)
results[c('data_stats', 'class_stats')] # Stats on the data
results$log # Log of the filtering procedure (if logging=TRUE)
results[c('stats', 'aov', 'tukey')] # Results of the statistical analysis
B_model = results$bf # The BF model

# Let's generate the plot of the posterior distirbutions
g_detailed = plot_posteriors(B_model) + ggtitle('Posterior distirbutions of effect size \n Alteration level semantic class.') + xlab('Effect size: CCNE1 KO AVANA score')


#######################
# Plot posteriors on PDF
pdf('CCNE1_CDKN2A_CCNE1.ko.AVANA_posteriors_effect_sizes_v1.pdf', width=5, height=3, useDingbats = FALSE)
print(g_class)
print(g_detailed)
dev.off()

