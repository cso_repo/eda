
#' Internal function. Log sample counts during filtering process
.log <- function(data, covariates) {
	require(dplyr)
	fgbg_stats = dplyr::count_(data, covariates) %>% dplyr::arrange(desc(n)) %>% data.frame
	return(fgbg_stats)
}

#' Internal function. Perform multivariate ANOVA analysis taking covariates into account
.anova_investigation <- function(data, v='v2') {
	# ANOVA test
	require(plyr)
	require(dplyr)
	require(tibble)
	# clean and check
	data = na.omit(data)
	if(nrow(data) < 3) { return(NULL) } # at least 3 samples to do a test
	if(length(unique(data$alt_class)) < 2) { return(NULL) } # at least 2 levels

	# By default, perform Tukey test without mutliple hypothesis correction
	if(v == 'v1') {
		# Do ANOVA
		paov = aov(effect ~ . + alt_class, data=data)
		pa = summary(paov)[[1]]
		pa = pa['alt_class','Pr(>F)']
		b = NULL
		if(!is.null(pa) && !is.na(pa)) {
			b = TukeyHSD(paov)$alt_class
			b = data.frame(b, stringsAsFactors = FALSE) %>% add_column(cmp = rownames(b)) # %>% arrange(p.adj)
		}
		# anova_stats = c('p.anova' = pa)
		# return(list('aov' = paov, 'stats' = anova_stats, 'tukey' = b))
	} else {
		require(multcomp)
		data$alt_class = factor(data$alt_class)
		paov = aov(effect ~ . + alt_class, data=data)
		pa = summary(paov)[[1]]
		pa = pa['alt_class','Pr(>F)']
		b = NULL
		if(!is.null(pa) && !is.na(pa)) {
			ht = glht(paov, linfct = mcp(alt_class = "Tukey"))
			ht = summary(ht, test = adjusted("none"))
			ht = data.frame('coefficient'=ht$test$coefficients, 't.stat'=ht$test$tstat, 'sigma'=ht$test$sigma, 'p.val'=ht$test$pvalues)
			# b = data.frame(b, stringsAsFactors = FALSE) %>% add_column(cmp = rownames(b)) # %>% arrange(p.adj)
			b = ht
		}
	}
	anova_stats = c('p.anova' = pa)
	return(list('aov' = paov, 'stats' = anova_stats, 'tukey' = b))
}

#' Internal function. Perform multivariate Bayesian analysis taking covariates into account
.bayesian_investigation <- function(data, ...) {
	# can pass parameters for lmBF
	require(BayesFactor)
	data = na.omit(data)
	# clean and check
	if(nrow(data) < 3) { return(NULL) } # at least 3 samples to do a test
	if(length(unique(data$alt_class)) < 2) { return(NULL) } # at least 2 levels 
	# Do BF
	if(ncol(data)>2) {
		bf_full = lmBF(effect ~ . + alt_class, data=data, progress=FALSE, ...)
		bf_tt = lmBF(effect ~ . -alt_class, data=data, progress=FALSE, ...)
		bf_alt_corr = extractBF(bf_full)$bf / extractBF(bf_tt)$bf
	} else {
		bf_full = lmBF(effect ~ alt_class, data=data, progress=FALSE, ...)
		bf_tt = NULL
		bf_alt_corr = extractBF(bf_full)$bf
	}
	# chain_full = posterior(bf_full, iterations=1000) # do not build if not necessary

	results = list(
		'model' = bf_full,
		'model_bg' = bf_tt,
		'stats' = c('bf' = bf_alt_corr)
		)
	return(results)
}

#' This function is the entrypoint for performing ANOVA and/or Bayesian analysis of association between a phenotype y and alteration profiles, taking covariates into account. 
#' The minimal required input is a dataframe with a column with the phenotype (y) and a column with the sample class, used to split the dataset in groups to be compared. Phenotype must be numeric, whereas  sample class is intended to be categorical.
#' @param data The only mandatory input, the dataframe with a column with the phenotype (y) and a column with the sample class (plus columns with covariates, if any)
#' @param effect_column column of data with the phenotype. Default: 'dep',
#' @param class_column column of data with the sample class. Default: 'alteration_class'
#' @param covariates vector of covariates to be accounted for in the analysis. Must be existing columns in the data dataframe
#' @param min.samples.per.class filter out classes with less than min.samples.per.class samples. Default: 0
#' @param do.anova logical, whether performing ANOVA analysis. Default: TRUE
#' @param do.bf logical, whether performing the Bayesian analysis. Default: TRUE
#' @param return.details logical, if TRUE the function returns data analysis details, such as the internal scores and samplings. Default: FALSE
#' @param logging logical, if TRUE returns an additional element with the number of samples retained throughout the analysis. Default: FALSE
#' @return Results of statistical analyis
#' @examples
#' \donttest{
#' 	profile_TP53 = collect_data('TP53', dataset, semantic)
#'  profile_BRAF = collect_data('BRAF', dataset, semantic)
#'  profile_TP53 = promote_cna_to_driver(df, type=c('DEL', 'MUT'), inclusive=FALSE)
#'  profile_BRAF = promote_cna_to_driver(df, type=c('AMP', 'MUT'), inclusive=FALSE)
#' 	profile_TP53_BRAF = merge_profiles(profiles = list('TP53' = profile_TP53, 'BRAF' = profile_BRAF), common_mask = c('sample_id'))
#'  semantic_profile_TP53_BRAF = merge_semantic(profile_TP53_BRAF, first='TP53', second='BRAF')
#'  results = basic_symmetric_multivariate_effect_analysis(semantic_profile_TP53_BRAF)
#' }
#'
basic_symmetric_multivariate_effect_analysis <- function(
	data,
	effect_column = 'dep',
	class_column = 'alteration_class',
	covariates=NULL,
	min.samples.per.class=0,
	do.anova = TRUE, do.bf = TRUE,
	return.details=FALSE,
	logging=FALSE,
	...) {
	require(plyr)
	require(dplyr)

	if(effect_column == 'alt_class' | class_column == 'effect') stop('Error. Column names are messed up.')

	proc.log = list()

	# print(head(data))
	# reduce data to interesting columns
	data$alt_class = data[, class_column]
	data$effect = data[, effect_column]
	data = data[, c('effect', covariates, 'alt_class'), drop=FALSE]
	if(logging) proc.log[['input']] = .log(data, c(covariates, 'alt_class'))

	# check and filter NAs
	fgbg = na.omit(data)
	if(logging) proc.log[['no.na']] = .log(fgbg, c(covariates, 'alt_class'))

# -----
# prepare for more advanced stat analysis
	# if(logging) proc.log[['alt_class_of_interest']] = .log(fgbg, c(covariates, 'alt_class'))
# --
# Remove rare classes and prepare for more robust analysis
	# for(i in covariates) { # remove NAs
	# 	fgbg = fgbg[!is.na(fgbg[,i]),,drop=FALSE]
	# }
	# if(logging) proc.log[['covariates_no.na']] = .log(fgbg, c(covariates, 'alt_class'))

# Filter!
	# temp = .inner_filter(data = fgbg, covariates = covariates, fg_class=fg_class, bg_class=bg_class, remove.empty.classes = remove.empty.classes, min.samples.per.class = min.samples.per.class, proc.log = proc.log, symm.filtering=FALSE)
	# fgbg = temp[['data']]
	# if(logging) proc.log = temp[['log']]
	classes.to.keep = fgbg %>% dplyr::count(alt_class) %>% filter(n >= min.samples.per.class) %>% pull(alt_class) %>% unique()
	fgbg = fgbg %>% filter(alt_class %in% classes.to.keep)

	if(length(unique(fgbg$alt_class)) < 2) { return(NULL) } # at least 2 levels to contrast are required!
	# if(length(which(fgbg$alt_class %in% fg_class)) < 3 | length(which(fgbg$alt_class %in% bg_class)) < 3) return(NULL)
	if(nrow(fgbg) < 3) return(NULL)

	# Remove covariates which do not have at least 2 classes
	for(i in covariates) { # remove covariates with only 1 category
		if(length(unique(fgbg[,i]))<2) fgbg[, i] = NULL
	}
	covariates = intersect(covariates, colnames(fgbg))
	if(logging) proc.log[['remove_constant_covariates']] = .log(fgbg, c(covariates, 'alt_class'))

	# print(fgbg[fgbg$alt_class == fg_class,])
#--
# some basic stats
	mfgs = sapply(split(fgbg, fgbg$alt_class), function(x) {
		median(x$effect, na.rm=TRUE)
	})
	ns = sapply(split(fgbg, fgbg$alt_class), function(x) {
		nrow(x)
	})
	fgbg_stats = dplyr::count_(fgbg, c(covariates, 'alt_class')) %>% dplyr::arrange(desc(n)) %>% data.frame

	# pw = NA # wilcox.test(fgbg$effect ~ fgbg$alt_class)$p.value # No sense in multivariate
	# if(length(unique(fgbg$alt_class))==2) pw = wilcox.test(fgbg$effect ~ fgbg$alt_class)$p.value

	# Invoke ANOVA test

	pa = NULL
	if(do.anova) pa = .anova_investigation(fgbg)

	# Invoke Bayesian test
	pb = NULL
	if(do.bf) pb = .bayesian_investigation(fgbg, ...)

	# Compact results
	stats_res = list('stats' = c(pa$stats, pb$stats))
	class_stats = cbind('n' = ns, 'median' = mfgs)
	stats_res$class_stats = class_stats

	if(return.details) {
		stats_res = list(
			'data' = fgbg,
			'data_stats' = fgbg_stats,
			'class_stats' = stats_res$class_stats,
			'stats' = stats_res$stats,
			'aov' = pa$aov, 'tukey' = pa$tukey,
			'bf' = pb$model,
			'log'=proc.log
		)
	}
	return(stats_res)
}


# ------   -------- #
#' Helper function. Tests multiple phenotypes versus the same sample classification / alteration profiles.
#' This function naively calls basic_symmetric_multivariate_effect_analysis (or a custom function passed as func parameter) iteratively on multiple phenotypes. Parallelization is supported.
#' @param x Dataframe of sample classification with a column with the sample class (plus columns with covariates, if any)
#' @param dataset table of phenotypes to be analyzed (phenotypes on columns, samples on rows).
#' @param func function to be called to perform the analysis. Default: basic_symmetric_multivariate_effect_analysis
#' @param effect_column not used. kept there to have interface similar to basic_symmetric_multivariate_effect_analysis
#' @param cl cluster to use for parallel processing, if any
#' @param n.cores number of cores to use if cl is null, a new cluster will be created
#' @param ... all the other parameters supported by function func
#' @return Results of statistical analyis on each phenotype
#' @examples
#' \donttest{
#' 	profile_TP53 = collect_data('TP53', dataset, semantic)
#'  profile_BRAF = collect_data('BRAF', dataset, semantic)
#'  profile_TP53 = promote_cna_to_driver(df, type=c('DEL', 'MUT'), inclusive=FALSE)
#'  profile_BRAF = promote_cna_to_driver(df, type=c('AMP', 'MUT'), inclusive=FALSE)
#' 	profile_TP53_BRAF = merge_profiles(profiles = list('TP53' = profile_TP53, 'BRAF' = profile_BRAF), common_mask = c('sample_id'))
#'  semantic_profile_TP53_BRAF = merge_semantic(profile_TP53_BRAF, first='TP53', second='BRAF')
#'  results = multiple_effect_analysis(semantic_profile_TP53_BRAF, ...)
#' }
#'

multiple_effect_analysis <- function(x, dataset, func = basic_symmetric_multivariate_effect_analysis, effect_column='unused', cl=NULL, n.cores=1, ...) {
	.effect_analysis <- function(x, k, func, ...) {
		x[, 'effect_column'] = k
		y = func(x, effect_column = 'effect_column', ...)
		return(y)
	}
	# Fix here check for data format and intersection of rownames
	dataset = dataset[rownames(x), , drop=FALSE] # need to reduce the set to common, using rownames for now
	require(foreach)
	if(n.cores>1 | !is.null(cl)) {
		require(parallel)
		require(doParallel)
		dostop = FALSE
		if(is.null(cl)) {
			stopImplicitCluster()
			cl <- makeCluster(n.cores, type="FORK")
			# cl <- makeCluster(n.cores, outfile=NULL)
			dostop = TRUE
		}
		registerDoParallel(cl)
		if( getDoParRegistered() ) {
			clusterExport(cl=cl, list("basic_symmetric_multivariate_effect_analysis", '.log', '.anova_investigation', '.bayesian_investigation')) #,envir=environment())
			print('--> [Beginning parallel computation ...]')
			nilbis <- foreach(k = 1:ncol(dataset), .export = c("basic_symmetric_multivariate_effect_analysis", '.log', '.anova_investigation', '.bayesian_investigation')) %dopar% .effect_analysis(x, k=dataset[,k], func = func, ...)
		} else {
			stop('Error in registering the parallel cluster.')
		}
		stopImplicitCluster()
		if(dostop) {
			stopCluster(cl)
		}
	} else {
		print('--> [Beginning sequential computation ...]')
		nilbis = lapply(1:ncol(dataset), function(k) {
			cat('.')
			.effect_analysis(x, dataset[, k], func = func, ...)
		})
	}
	nil = nilbis
	names(nil) = colnames(dataset)
	nil = nil[!sapply(nil,is.null)] # remove NAs already
	return(nil)
}



# ------   -------- #
#' Helper function. Tests multiple associations in parallel.
#' This function naively calls basic_symmetric_multivariate_effect_analysis (or a custom function passed as func parameter) iteratively on a list of dataframes. Parallelization is supported.
#' @param x list of Dataframe of sample classifications with a column with the sample class (plus columns with covariates, if any) and a column with the phenotype.
#' @param func function to be called to perform the analysis. Default: basic_symmetric_multivariate_effect_analysis
#' @param cl cluster to use for parallel processing, if any
#' @param n.cores number of cores to use if cl is null, a new cluster will be created
#' @param ... all the other parameters supported by function func
#' @return Results of statistical analyis on each input case
#' @examples
#' \donttest{
#' 	profile_TP53 = collect_data('TP53', dataset, semantic)
#'  profile_BRAF = collect_data('BRAF', dataset, semantic)
#'  profile_TP53 = promote_cna_to_driver(df, type=c('DEL', 'MUT'), inclusive=FALSE)
#'  profile_BRAF = promote_cna_to_driver(df, type=c('AMP', 'MUT'), inclusive=FALSE)
#' 	profile_TP53_BRAF = merge_profiles(profiles = list('TP53' = profile_TP53, 'BRAF' = profile_BRAF), common_mask = c('sample_id'))
#'  semantic_profile_TP53_BRAF = merge_semantic(profile_TP53_BRAF, first='TP53', second='BRAF')
#'  results = parallel_effect_analysis(...)
#' }
#'
parallel_effect_analysis <- function(x, func = basic_symmetric_multivariate_effect_analysis, cl=NULL, n.cores=1, ...) {
	.effect_analysis <- function(x, func, ...) {
		# print(list(...))
		y = func(x, ...)
		return(y)
	}
	# Fix here check for data format and intersection of rownames
	require(foreach)
	if(n.cores>1 | !is.null(cl)) {
		require(parallel)
		# require(doParallel)
		dostop = FALSE
		if(is.null(cl)) {
			stopImplicitCluster()
			cl <- makeCluster(n.cores, type="FORK")
			# cl <- makeCluster(n.cores, outfile=NULL)
			dostop = TRUE
		}
		# registerDoParallel(cl)
		# if( getDoParRegistered() ) {
		clusterExport(cl=cl, list("basic_symmetric_multivariate_effect_analysis", '.log', '.anova_investigation', '.bayesian_investigation')) #,envir=environment())
		print('--> [Beginning parallel computation ...]')

		# library("future.apply")
		# nilbis = future_lapply(x, function(x) .effect_analysis(x, func = func, ...))
		other_params = list(...)
		other_params[['func']] = func
		nilbis = parLapply(cl, x, function(x, other_params) {
			other_params[['x']] = x
			# print(other_params)
			do.call(.effect_analysis, other_params)
			# other_params
			}, other_params)

		# nilbis <- foreach(k = 1:length(x), .export = c("basic_symmetric_multivariate_effect_analysis", '.log', '.anova_investigation', '.bayesian_investigation')) %dopar% .effect_analysis(x[[k]], func = func, ...)
		# } else {
			# stop('Error in registering the parallel cluster.')
		# }
		# stopImplicitCluster()
		if(dostop) {
			stopCluster(cl)
		}
	} else {
		print('--> [Beginning sequential computation ...]')
		nilbis = lapply(x, function(x) {
			cat('.')
			.effect_analysis(x, func = func, ...)
		})
	}
	nil = nilbis
	nil = nil[!sapply(nil,is.null)] # remove NAs already
	return(nil)
}
